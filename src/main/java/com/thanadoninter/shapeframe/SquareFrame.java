/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Acer
 */
public class SquareFrame extends JFrame {

    public SquareFrame() {
        super("Square");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(640, 360);
        this.setLayout(null);

        JLabel lblSide = new JLabel("Side:", JLabel.CENTER);
        lblSide.setSize(80, 30);
        lblSide.setLocation(30, 30);
        lblSide.setBackground(Color.magenta);
        lblSide.setOpaque(true);
        this.add(lblSide);

        final JTextField txtSide = new JTextField();
        txtSide.setSize(200, 30);
        txtSide.setLocation(120, 30);
        this.add(txtSide);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(200, 60);
        btnCalculate.setLocation(120, 80);
        this.add(btnCalculate);

        final JLabel lblResult = new JLabel("", JLabel.CENTER);
        lblResult.setSize(640, 60);
        lblResult.setLocation(0, 150);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strSide = txtSide.getText();//รับค่าด้านจากText
                    double side = Double.parseDouble(strSide);//แปลงสตริงให้เป็นดับเบิ้ล
                    Square square = new Square(side);//สร้างจัตุรัส
                    lblResult.setText("Square: Side = " + String.format("%.2f", square.getSide())
                            + ", Area = " + String.format("%.2f", square.calArea())
                            + ", Perimiter = " + String.format("%.2f", square.calPerimiter()));

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(SquareFrame.this, "Error: Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtSide.setText("");
                    txtSide.requestFocus();
                }
            }
        });
    }

    public static void main(String[] args) {
        SquareFrame frame = new SquareFrame();
        frame.setVisible(true);
    }
}
