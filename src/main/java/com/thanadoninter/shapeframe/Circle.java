/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.shapeframe;

/**
 *
 * @author Acer
 */
public class Circle extends Shape {
    private double radius;

    public Circle(double radius) {
        super("Circle");
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
    
    @Override
    public double calArea() {
        return Math.PI*this.radius*this.radius;
    }

    @Override
    public double calPerimiter() {
        return 2*Math.PI*radius;
    }
    
}
