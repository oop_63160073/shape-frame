/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Acer
 */
public class TriangleFrame extends JFrame {

    public TriangleFrame() {
        super("Triangle");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(640, 360);
        this.setLayout(null);

        JLabel lblSide1 = new JLabel("Side1:", JLabel.CENTER);
        lblSide1.setSize(80, 30);
        lblSide1.setLocation(30, 30);
        lblSide1.setBackground(Color.magenta);
        lblSide1.setOpaque(true);
        this.add(lblSide1);

        JLabel lblSide2 = new JLabel("Side2:", JLabel.CENTER);
        lblSide2.setSize(80, 30);
        lblSide2.setLocation(30, 70);
        lblSide2.setBackground(Color.magenta);
        lblSide2.setOpaque(true);
        this.add(lblSide2);

        JLabel lblBase = new JLabel("Base:", JLabel.CENTER);
        lblBase.setSize(80, 30);
        lblBase.setLocation(30, 110);
        lblBase.setBackground(Color.magenta);
        lblBase.setOpaque(true);
        this.add(lblBase);

        JLabel lblHeight = new JLabel("Height:", JLabel.CENTER);
        lblHeight.setSize(80, 30);
        lblHeight.setLocation(30, 150);
        lblHeight.setBackground(Color.magenta);
        lblHeight.setOpaque(true);
        this.add(lblHeight);

        final JTextField txtSide1 = new JTextField();
        txtSide1.setSize(200, 30);
        txtSide1.setLocation(120, 30);
        this.add(txtSide1);

        final JTextField txtSide2 = new JTextField();
        txtSide2.setSize(200, 30);
        txtSide2.setLocation(120, 70);
        this.add(txtSide2);

        final JTextField txtBase = new JTextField();
        txtBase.setSize(200, 30);
        txtBase.setLocation(120, 110);
        this.add(txtBase);

        final JTextField txtHeight = new JTextField();
        txtHeight.setSize(200, 30);
        txtHeight.setLocation(120, 150);
        this.add(txtHeight);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(200, 60);
        btnCalculate.setLocation(120, 190);
        this.add(btnCalculate);

        final JLabel lblResult = new JLabel("", JLabel.CENTER);
        lblResult.setSize(640, 60);
        lblResult.setLocation(0, 260);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strSide1 = txtSide1.getText();//รับค่าด้านจากText
                    String strSide2 = txtSide2.getText();
                    String strBase = txtBase.getText();
                    String strHeight = txtHeight.getText();
                    double side1 = Double.parseDouble(strSide1);//แปลงสตริงให้เป็นดับเบิ้ล
                    double side2 = Double.parseDouble(strSide2);
                    double base = Double.parseDouble(strBase);
                    double height = Double.parseDouble(strHeight);
                    Triangle triangle = new Triangle(side1, side2, base, height);//สร้างสีเหลี่ยมพื้นผ้า
                    lblResult.setText("Triangle: Side1 = " + String.format("%.2f", triangle.getSide1())
                            + ", Side2 = " + String.format("%.2f", triangle.getSide2())
                            + ", Base = " + String.format("%.2f", triangle.getBase())
                            + ", Height = " + String.format("%.2f", triangle.getHeight())
                            + ", Area = " + String.format("%.2f", triangle.calArea())
                            + ", Perimiter = " + String.format("%.2f", triangle.calPerimiter()));

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(TriangleFrame.this, "Error: Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtSide1.setText("");
                    txtSide2.setText("");
                    txtBase.setText("");
                    txtHeight.setText("");
                    txtSide1.requestFocus();
                    txtSide2.requestFocus();
                    txtBase.requestFocus();
                    txtHeight.requestFocus();
                }
            }
        });
    }

    public static void main(String[] args) {
        TriangleFrame frame = new TriangleFrame();
        frame.setVisible(true);
    }
}
