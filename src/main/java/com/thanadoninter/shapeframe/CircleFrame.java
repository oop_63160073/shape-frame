/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Acer
 */
public class CircleFrame extends JFrame {

    public CircleFrame() {
        super("Circle");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(640, 360);
        this.setLayout(null);

        JLabel lblRadius = new JLabel("Radius:", JLabel.CENTER);
        lblRadius.setSize(80, 30);
        lblRadius.setLocation(30, 30);
        lblRadius.setBackground(Color.magenta);
        lblRadius.setOpaque(true);
        this.add(lblRadius);

        final JTextField txtRadius = new JTextField();
        txtRadius.setSize(200, 30);
        txtRadius.setLocation(120, 30);
        this.add(txtRadius);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(200, 60);
        btnCalculate.setLocation(120, 80);
        this.add(btnCalculate);

        final JLabel lblResult = new JLabel("", JLabel.CENTER);
        lblResult.setSize(640, 60);
        lblResult.setLocation(0, 150);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strRadius = txtRadius.getText();//รับค่ารัศมีจากText
                    double radius = Double.parseDouble(strRadius);//แปลงสตริงให้เป็นดับเบิ้ล
                    Circle circle = new Circle(radius);//สร้างวงกลม
                    lblResult.setText("Circle: Radius = " + String.format("%.2f", circle.getRadius())
                            + ", Area = " + String.format("%.2f", circle.calArea())
                            + ", Perimiter = " + String.format("%.2f", circle.calPerimiter()));

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(CircleFrame.this, "Error: Please input number",
                             "Error", JOptionPane.ERROR_MESSAGE);
                    txtRadius.setText("");
                    txtRadius.requestFocus();
                }
            }
        });
    }

    public static void main(String[] args) {
        CircleFrame frame = new CircleFrame();
        frame.setVisible(true);
    }
}
