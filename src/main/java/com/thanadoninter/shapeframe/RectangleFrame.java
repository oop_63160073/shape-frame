/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Acer
 */
public class RectangleFrame extends JFrame {

    public RectangleFrame() {
        super("Rectangle");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(640, 360);
        this.setLayout(null);

        JLabel lblWidth = new JLabel("Width:", JLabel.CENTER);
        lblWidth.setSize(80, 30);
        lblWidth.setLocation(30, 30);
        lblWidth.setBackground(Color.magenta);
        lblWidth.setOpaque(true);
        this.add(lblWidth);

        JLabel lblHeight = new JLabel("Height:", JLabel.CENTER);
        lblHeight.setSize(80, 30);
        lblHeight.setLocation(30, 70);
        lblHeight.setBackground(Color.magenta);
        lblHeight.setOpaque(true);
        this.add(lblHeight);

        final JTextField txtWidth = new JTextField();
        txtWidth.setSize(200, 30);
        txtWidth.setLocation(120, 30);
        this.add(txtWidth);

        final JTextField txtHeight = new JTextField();
        txtHeight.setSize(200, 30);
        txtHeight.setLocation(120, 70);
        this.add(txtHeight);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(200, 60);
        btnCalculate.setLocation(120, 110);
        this.add(btnCalculate);

        final JLabel lblResult = new JLabel("", JLabel.CENTER);
        lblResult.setSize(640, 60);
        lblResult.setLocation(0, 180);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strWidth = txtWidth.getText();//รับค่าด้านจากText
                    String strHeight = txtHeight.getText();
                    double width = Double.parseDouble(strWidth);//แปลงสตริงให้เป็นดับเบิ้ล
                    double height = Double.parseDouble(strHeight);
                    Rectangle rectangle = new Rectangle(width, height);//สร้างสีเหลี่ยมพื้นผ้า
                    lblResult.setText("Rectangle: Width = " + String.format("%.2f", rectangle.getWidth())
                            + ", Height = " + String.format("%.2f", rectangle.getHeight())
                            + ", Area = " + String.format("%.2f", rectangle.calArea())
                            + ", Perimiter = " + String.format("%.2f", rectangle.calPerimiter()));

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(RectangleFrame.this, "Error: Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText("");
                    txtHeight.setText("");
                    txtWidth.requestFocus();
                    txtHeight.requestFocus();
                }
            }
        });
    }

    public static void main(String[] args) {
        RectangleFrame frame = new RectangleFrame();
        frame.setVisible(true);
    }
}
